package teamawesome;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MouseInfo;
import java.awt.Rectangle;

import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.JPanel;
import javax.swing.event.MouseInputAdapter;


public class Board extends JPanel implements Runnable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String boardImage = "images/boardImage.png";
	private Image image;
	
	Map map = new Map();
	Player player = new Player("You");
	Gun Weapon;
	
	
	
	
	
	
	String versionNumber = "Alpha 0.0.1 R10";
	Client client;
	int updates = 0;
	Bullet[] bullets;
	int numBullets;
	public boolean up;
	public boolean down;
	public boolean left;
	public boolean right;
	Rectangle PlayerBarrier;
	private ArrayList BarrierRec;
			 
	private ArrayList BarrierClass;
	
	public Board() throws IOException{
		//BOARD CODE//
		//THIS IS EVERYTHING INSIDE THE GAME WINDOW//
		client = new Client("You");
		client.sendCommand(player,map,"Login",0);
		bullets = new Bullet[41];	//ARRAY FOR THE BULLETS//
		numBullets = 0;				//NONE ON THE SCREEN FOR INIT//
		

		setBackground(Color.BLUE);
		
		//KEY DOWN
		getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put( KeyStroke.getKeyStroke( "W" ),"GOUP" );
        getActionMap().put("GOUP", new GOUP()); //Keystroke detector UP
        getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put( KeyStroke.getKeyStroke( "S" ),"GODOWN" );
        getActionMap().put("GODOWN", new GODOWN()); //Keystroke detector DOWN
        getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put( KeyStroke.getKeyStroke( "A" ),"GOLEFT" );
        getActionMap().put("GOLEFT", new GOLEFT()); //Keystroke detector LEFT
        getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put( KeyStroke.getKeyStroke( "D" ),"GORIGHT" );
        getActionMap().put("GORIGHT", new GORIGHT()); //Keystroke detector RIGHT
        getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put( KeyStroke.getKeyStroke( "1" ),"Weapon1" );
        getActionMap().put("Weapon1", new Weapon1()); //Keystroke detector 1 Change Weapon
        getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put( KeyStroke.getKeyStroke( "2" ),"Weapon2" );
        getActionMap().put("Weapon2", new Weapon2()); //Keystroke detector 2 Change Weapon
        //KEY RELEASE
        getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put( KeyStroke.getKeyStroke( "released W"),"releasedUp");
        getActionMap().put("releasedUp", new releasedUp()); //Keystroke detector UP
        getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put( KeyStroke.getKeyStroke( "released S" ),"releasedDown" );
        getActionMap().put("releasedDown", new releasedDown()); //Keystroke detector DOWN
        getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put( KeyStroke.getKeyStroke( "released A" ),"releasedLeft" );
        getActionMap().put("releasedLeft", new releasedLeft()); //Keystroke detector LEFT
        getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put( KeyStroke.getKeyStroke( "released D" ),"releasedRight" );
        getActionMap().put("releasedRight", new releasedRight()); //Keystroke detector RIGHT
        //MOUSE LISTENERS
        MouseListener MouseListener = new MouseListener();
        addMouseListener(MouseListener);
        addMouseMotionListener(MouseListener);
        
        Thread thread = new Thread(this); //THIS IS THE NEW GAME THREAD
        thread.start();
        SetWeaponStart();
	}
	
	

	
	
		public void UpdateBarriers()
		{
			//DRAW BARRIERS FOR COLLISSION
			PlayerBarrier = new Rectangle((int)player.getX(), (int)player.getY(), 18, 18);
			BarrierClass = new ArrayList();
			BarrierRec = new ArrayList();
			BarrierClass.add( new Barrier(map.getX()+300,map.getY()+300));
			//Create Rectangles for collision testing
			
			BarrierRec.add(new Rectangle((int)map.getX()+55,(int)map.getY()+55,890,1));//top
			BarrierRec.add(new Rectangle((int)map.getX()+55,(int)map.getY()+55,1,595));//left
			
			BarrierRec.add(new Rectangle((int)map.getX()+945,(int)map.getY()+55,1,595));//right
			BarrierRec.add(new Rectangle((int)map.getX()+55,(int)map.getY()+645,890,1));//bottom
			
			//SET RECTANGLE AROUND CAR
			for(int bc= 0; bc <BarrierClass.size();bc++)
			{
				//NEED TO RE-THINK THE WAY IN WHICH IT GETS THE X Y TO BE BASED AROUND THE BARRIER POSITION RATHER THAN MAP
				//THUS WE NEED TO UPDATE THE BARRIERCLASS LOCATION WHEN THE MAP MOVES
				Barrier bcc = (Barrier)BarrierClass.get(bc);
				BarrierRec.add(new Rectangle((int)map.getX()+300,(int)map.getY()+300,(int)bcc.getwidth(),(int)bcc.getheight()));//Car
			}
			
		}
	@Override
	public void paint(Graphics g){
		
		
		UpdateBarriers();
		//PAINT METHOD/
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		Font font = new Font("Tahoma",Font.PLAIN,16);
		Font font2 = new Font("Tahoma",Font.BOLD,12);
		Font versionFont = new Font("Courier New", Font.BOLD, 12);
		g2d.setFont(font);
		g2d.setColor(Color.WHITE);
		g2d.drawImage(map.getImage(), (int)map.getX(),(int)map.getY(), this);
		g2d.drawString("Team Awesome Paintball", 10, 20);
		AffineTransform at = new AffineTransform();
		at.translate(player.getX(),player.getY());
		
		at.rotate(Math.toRadians(player.getRotationAngle()),18/2,18/2);
		g2d.drawImage(player.getImage(), at, this);
		
		//DRAW IMAGES FOR THE BARRIER CLASS BASSED ON WHAT IS IN THE ARRAY
		for(int j = 0; j < BarrierClass.size(); j++)
		{
			Barrier ba = (Barrier)BarrierClass.get(j);
			g2d.drawImage(ba.getImage(),(int)map.getX()+300,(int)map.getY()+300, null, this);
		}
		
		Color Transparent = new Color(255, 0,0, 0 );
		g2d.setColor(Transparent);
		
		g2d.draw(PlayerBarrier);
		
		//DRAW BARRIERS AROUND BARRIER CLASSES BASED ON WHAT IS IN 2ND BARRIER ARRAY
		for(int t = 0; t < BarrierRec.size(); t++)
		{
			Rectangle b = (Rectangle)BarrierRec.get(t);
			g2d.draw(b);
		}
		
		g2d.setColor(Color.RED);
		g2d.setFont(font2);
		g2d.drawString(player.getUser(),(int)player.getX() - 5,(int)player.getY() -10);
		g2d.setFont(versionFont);
		g2d.setColor(Color.CYAN);
		g2d.drawString("Version: " + versionNumber, 720, 10);
		g2d.drawString("Updates: " + updates, 720,25);
		g2d.drawString("Bullets: " + numBullets, 720, 40);
		g2d.drawString("Player ID: " + client.getPlayerID(), 710, 60);
		checkCollision();
		
		for(int i=0;i<numBullets;i++)
			bullets[i].draw(g2d);
	}
	

	@Override
	public void run() {
		for(;;){
				///////////////////////////////
				//GAME LOGIC NOW GOES HERE   //
				//YES, THAT NOW INCLUDES     //
				//ANYTHING TO DO WITH 	     //
				//MOVING STUFF			     //
				///////////////////////////////
			
			long startTime = System.currentTimeMillis();
			
			repaint();
		
			if(up)
			{
				map.moveDown();
				try {
					client.sendCommand(player, map, "UpdatePosition", 0);
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(down)
			{
				map.moveUp();
				try {
					client.sendCommand(player, map, "UpdatePosition", 0);
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(left)
			{
				map.moveRight();
				try {
					client.sendCommand(player, map, "UpdatePosition", 0);
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(right)
			{
				map.moveLeft();
				try {
					client.sendCommand(player, map, "UpdatePosition", 0);
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			
			try{
				long endTime = System.currentTimeMillis();
				
				long framePeriod = 5;
	
				if(framePeriod-(endTime-startTime)>0)
					Thread.sleep(framePeriod -
					(endTime-startTime));
				
			}catch(InterruptedException e){
				
			}
			
			for(int i=0;i<numBullets;i++){
				bullets[i].move();
				if(bullets[i].getLife()<=0){ 
					//shifts all the next shots up one 
					//space in the array
					 deleteShot(i); //SEE NEW METHOD BELOW
					 i--; // move the outer loop back one so 
					// the shot shifted up is not skipped
					 } 
			}
			updates++;
		}
	}
	private void deleteShot(int index){ 
		//delete shot and move all shots after it up in the array
		numBullets--; 
		for(int i=index;i<numBullets;i++) 
		bullets[i]=bullets[i+1]; 
		bullets[numBullets]=null; 
		} 
	
	public String oxbp;
	public String oybp;
	public void checkCollision(){
		
		for(int bl = 0; bl < numBullets; bl++)
		{
			
			if(bullets[bl].GetBulletRec().intersects(PlayerBarrier))
			{
				//player.setHealth(bullets[bl].getDmg());
				//bullets[bl].life = bullets[bl].life -40;
				//deleteShot(bl);
			}
		}
		
		for(int t = 0; t < BarrierRec.size(); t++)
		{
			Rectangle Bar = (Rectangle)BarrierRec.get(t);

			for(int bl = 0; bl < numBullets; bl++)
			{
				if(Bar.intersects(bullets[bl].GetBulletRec()))
				{
					deleteShot(bl);
				}
				
			}
			
			
			if(Bar.intersects(PlayerBarrier))
			{
				    if (left == true)
				    {
				        map.setX(map.getX() -5);
				    }
				    if (right == true)
				    {
				        map.setX(map.getX() +5);
				    }
				    if (up == true)
				    {
				    	map.setY(map.getY() -5);
				    }
				    if (down == true)
				    {
				    	map.setY(map.getY() +5);
				    }
				    up = false;
					down= false;
					left = false;
					right = false;
			}
		}

	}
	
	
	public class Weapon1 extends AbstractAction{

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent arg0) {
			 Weapon = new Pistol();
			SetWeapon();
		}
		
	}
	public class Weapon2 extends AbstractAction{

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent arg0) {
			 Weapon = new Sniper();
			SetWeapon();
			
		}
		
	}
	
	public class releasedRight extends AbstractAction{

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent arg0) {
			right = false;
			
		}
		
	}
	public class releasedLeft extends AbstractAction{

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent arg0) {
			left = false;
			
			
		}
		
	}
	public class releasedUp extends AbstractAction{

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent arg0) {
			up = false;
			
		}
		
	}
	
	public class releasedDown extends AbstractAction{


		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			down = false;
		
			
		}
		
	}
	
	public class GOUP extends AbstractAction{
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			checkCollision();
				up = true;
				checkCollision();
		}
		
	}
	public class GODOWN extends AbstractAction{
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			checkCollision();
				down = true;
				checkCollision();
			
			
		}
		
	}
	public class GOLEFT extends AbstractAction{
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			checkCollision();
				left = true;
				checkCollision();
			
		}
		
	}
	public class GORIGHT extends AbstractAction{
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			checkCollision();
				right = true;
				checkCollision();
			
		}
		
	}
	
	//MOUSE EVENT LISTENING CLASS
	public class MouseListener extends MouseInputAdapter {
		//MOUSE PRESSED
	    @Override
		public void mousePressed(MouseEvent e) {
	       
	        bullets[numBullets] = player.shoot();
	        numBullets++;
	        
	    }
	    //MOUSE MOVED
	    @Override
		public void mouseMoved(MouseEvent e) {
	        int x = e.getX();
	        int y = e.getY();
	       player.Rotate(x,y);
	       try {
			client.sendCommand(player, map, "UpdatePosition", 0);
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    }
	    }
	  
    public int Getmy()
    {
        int mouseY = MouseInfo.getPointerInfo().getLocation().y;
		return mouseY;
    }
    public int Getmx()
    {
        int mousex = MouseInfo.getPointerInfo().getLocation().x;
		return mousex;
    }         
    
  //Set Weapon 
	private void  SetWeapon()
  	{
		
  		SetBulDamge(Weapon.GetBulDamge());
  		SetBulLife(Weapon.GetBulLife());
  		SetBulSpeed(Weapon.GetBulSpeed());
  		SetBulCol(Weapon.GetBulImage());
  	}
  	private void  SetWeaponStart()
  	{
  		 Weapon = new Pistol();
  		SetBulDamge(Weapon.GetBulDamge());
  		SetBulLife(Weapon.GetBulLife());
  		SetBulSpeed(Weapon.GetBulSpeed());
  		SetBulCol(Weapon.GetBulImage());
  	}
  	//Set Damage
  	public void SetBulDamge(double Dmg){
  		
  		player.setdmg(Dmg);
  		
  	}
  	//Set Bullet Life
  	public void SetBulLife(int life){
  			
  		player.setBulLife(life);
  		
  	}
  	//Set Bullet Speed
  	public void SetBulSpeed(double Spd){
  		
  		player.setBulSpd(Spd);
  		
  	}

  	public void SetBulCol(Color Col){
  		
  		player.SetBulCol(Col);
  		
  	}

    
    

	}


