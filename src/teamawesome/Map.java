package teamawesome;

import java.awt.Image;


import javax.swing.ImageIcon;

public class Map {
	private String map = "map.png";
	private double x;
	private double y;
	private Image image;
	public Map(){
		super();
		ImageIcon ii = new ImageIcon(this.getClass().getResource(map));
		image = ii.getImage();
		x = 0;
		y = 0;
	}
	
	public Image getImage(){
		return image;
	}
	
	public double getX(){
		return x;
	}
	
	public double getY(){
		return y;
	}
	public void setX(double nx){
		x = nx;
	}
	public void setY(double ny){
		y = ny;
	}
	
	public void moveUp(){
		y= y-2;
	}
	public void moveDown(){
		y = y +2;
	}
	public void moveLeft(){
		x = x -2;
	}
	public void moveRight(){
		x = x + 2;
	}

	
}
