package teamawesome;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;
import java.util.HashMap;

public class Client{
	HashMap<Integer,Player> cPlayerMap;
	HashMap<Integer, Bullet> cBulletMap;
	int serverPort = 9876;
	String ip = "37.59.196.83";
	Socket s = null;
	byte[] eOutput = null;
    byte[] sendData = new byte[1024];
    byte[] receiveData = new byte[1024];
	int offset = 0;
	private String username;
	private int playerID;
	
	public Client(String username) throws UnknownHostException, SocketException{
		this.username = username;
		
		
	}
	
	public void sendCommand(Player player, Map map, String Command, int offset) throws UnknownHostException, IOException{
		InetAddress IPAddress = InetAddress.getByName("mattarnster.co.uk");
		DatagramSocket clientSocket = new DatagramSocket();
		if(Command == "Login"){
			String sOutput = Command + "," +player.getUser() + "," + player.getX() + "," + player.getY() 
					+ "," + player.getRotationAngle() + "," + player.getHealth();
			eOutput = sOutput.getBytes(); 
		}
		
		if(Command == "UpdatePosition"){
			
			String sOutput = Command + "," + playerID + "," + map.getX() + "," + map.getY()
					+ "," + player.getRotationAngle();
			eOutput = sOutput.getBytes();
			NetworkUpdate nu = new NetworkUpdate(player,map,eOutput);
			return;
		}
		
		if(Command == "GetRemotePlayers"){
			String sOutput = Command + offset;
			eOutput = sOutput.getBytes();
		}
		
		if(Command == "GetNumPlayers"){
			String sOutput = Command;
			eOutput = sOutput.getBytes();
		}
		
		if(Command == "GetRemoteBullets"){
			String sOutput = Command;
			eOutput = sOutput.getBytes();
		}
		DatagramPacket sendPacket = new DatagramPacket(eOutput, eOutput.length, IPAddress, serverPort);
        clientSocket.send(sendPacket);
		DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        clientSocket.receive(receivePacket);
        String serverResponse = processPacket(receivePacket);
        String modifiedSentence = new String(receivePacket.getData());
        clientSocket.close();
        
        System.out.println(serverResponse);
        String sServerResponse[] = serverResponse.split(","); 
        if(sServerResponse[0].equals("Login")){
        	System.out.println(sServerResponse[1]);
        	playerID = Integer.parseInt(sServerResponse[1]);
        }
        if(sServerResponse[0].equals("UpdatePosition")){
        	System.out.println("Position updated");
        }
        
	}
	public int getPlayerID(){
		return playerID;
	}
	public static String processPacket(DatagramPacket packet){
	       String result = new String(packet.getData());
	       char[] annoyingchar = new char[1];
	       char[] charresult = result.toCharArray();
	       result = "";
	       for(int i=0;i<charresult.length;i++){
	           if(charresult[i]==annoyingchar[0]){
	               break;
	           }
	           result+=charresult[i];
	       }
	       return result;
	   }
}
