package teamawesome;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;

public class Bullet  {
	double shotSpeed;//Shot Speed
	double x;//This x
	double y;//This Y
	double angle;//Angle to Mouse
	double dx;//Mouse X
	double dy;//Mouse Y
	int life;//Distance
	float xVelocity;
	private Rectangle bulletRectangle;
	float yVelocity;
	private double dmg ;
	private Color color;
	public Bullet(double x, double y, double dx, double dy, int life, double spd, double dmg,Color Col){
		/*
		 * x - X position of the player
		 * y - Y position of the player
		 * angle - Angle player is facing
		 * dx - X Destination of the click 
		 * dy - Y Destination of the click 
		 */
		 this.x = x;
		 this.y = y;
		
		 this.life = life;
		 this.dmg = dmg;
		 this.shotSpeed = spd;
		 this.color = Col;
		 this.dx = dx;
		 this.dy = dy;
		 this.bulletRectangle = new Rectangle((int)x,(int)y,5,5);
		 this.angle =  Math.toDegrees(Math.atan2((dy-y), (dx-x)));
		 
		 
		 this.xVelocity=(float) (Math.cos(Math.toRadians((angle)))*shotSpeed);
		 this.yVelocity=(float) (Math.sin(Math.toRadians((angle)))*shotSpeed);;
		 
		 
	}
	
	
	public double getDmg()
	{
		return dmg;
	}
	public void draw(Graphics2D g2d) {	
		
	
		g2d.setColor(color);
		g2d.fillOval((int)(x), (int)(y), 5, 5);
		
		bulletRectangle = new Rectangle((int)x+2,(int)y+2,5,5);
		Color Transparent = new Color(255, 0,0, 0 );
		g2d.setColor(Transparent);
		g2d.draw(bulletRectangle);
		g2d.setColor(color);
	}
	public int getLife(){
		return life;
	}
	public void move(){
		life--; // used to make shot disappear if it goes too long
		// without hitting anything
		x+=xVelocity; // move the shot
		y+=yVelocity;
	
	}
	
	public double getX(){
		return x;
	}
	public double getY(){
		return y;
	}
	
	public Rectangle GetBulletRec()
	{
		return bulletRectangle;
	}
}
