package teamawesome;

import java.awt.Color;
import java.awt.Image;


import javax.swing.ImageIcon;

public class Player {
	public String player = "player.png";
	private double x;
	private double y;
	private float rotateAngle;
	private Image image;
	private String user;
	private double omx;
	private double omy;
	private double health = 100;
	private Color color;
	private double bullDmg;
	private double bullSpd;
	private int BullLif;
	
	public Player(String username){
		super();
		user = username;
		ImageIcon ii = new ImageIcon(this.getClass().getResource(player));
		image = ii.getImage();
		x = 430;
		y = 300;
		
	}
	
	public Image getImage(){
		return image;
	}
	
	public double getX(){
		return x;
	}
	
	public double getY(){
		return y;
	}
	public double getHealth(){
		return health;
	}
	
	
	//SERVER SIDE
	public void setX(double nx){
		x = nx;
	}
	public void setY(double ny){
		y = ny;
	}
	
	
	public String getUser(){
		return user;
	}
	
	
	
	public void setdmg(double dmg)
	{
		bullDmg = dmg;
	}
	public void setBulLife(int lf){
		BullLif = lf;
	}
	public void setBulSpd(double spd){
		bullSpd = spd;
	}
	
	
	////////////////////////////////////////
	//REMOVE THIS WHEN WE HAVE BULLET IMAGES
	public void SetBulCol(Color col)
	{
		color = col;
	}
	//////////////////////////////////////
	
	public Bullet shoot(){
		return new Bullet(getX()+10,getY()+6,omx,omy,BullLif,bullSpd,bullDmg,color);
	}
	
	
	public void Rotate(double mx, double my) {
		
		rotateAngle = (float) Math.atan2(my-y, mx - x);
		rotateAngle = (float) Math.toDegrees(rotateAngle) + 90;
		omx = mx;
		omy = my;
	}
	
	public double getRotationAngle(){
		return rotateAngle ;
	}

	public void setHealth(double dmg) {
		// TODO Auto-generated method stub
		health = health - dmg;
		
		if(health <= 0)
		{
			System.out.println("dead");
		}
	}
	
	
	
}
