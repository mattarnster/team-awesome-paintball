package teamawesome;


import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.Image;



public class Barrier {
	public String barrier = "BarrierImgTest.png";

	private Image image;
	private double y;
	private double x;
	private int width;
	private int height;
	

	
	public Barrier(double nx, double ny){
		super();
		
		ImageIcon ii = new ImageIcon(this.getClass().getResource(barrier));
		image = ii.getImage();
		x = nx;
		y = ny;
		 width = ii.getIconWidth();
		 height = ii.getIconHeight();
	}
	public Image getImage(){
		return image;
	}
	public double getX(){
		return x;
	}
	
	public double getY(){
		return y;
	}
	public double getwidth(){
		return width;
	}
	
	public double getheight(){
		return height;
	}
	
	


}
