package teamawesome;

import java.awt.Color;

public class Gun {
	private double speed = 3;
	private double damage =10;
	private int life =80;
	private Color color;
	public double GetBulDamge()
	{
		return damage;
	}
	public double GetBulSpeed()
	{
		return speed;
	}
	public int GetBulLife()
	{
		return life;
	}
	

	public Color GetBulImage()
	{
		return color;
	}

	
	
	public void SetBulImage(Color img)
	{
		color = img;
	}
	public void SetBulDamge(double dmg)
	{
		damage = dmg;
		
	}
	public void SetBulSpeed(double spd)
	{
		speed = spd;
	}
	public void SetBulLife(int lf)
	{
		life = lf;
	}
}