package teamawesome;

import java.applet.Applet;
import java.io.IOException;

import javax.swing.JFrame;

public class Skeleton extends JFrame{
	public Skeleton() throws IOException{
		//SKELETON CODE//
		
		//THIS IS WHERE THE GAME INITIALISES AND DRAWS THE GAME WINDOW//
		add(new Menu());
		setTitle("Team Awesome Paintball");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(900, 600);
        setFocusable(true);
        setLocationRelativeTo(null);
        setVisible(true);
        setResizable(false);
	}
	
	public static void main(String[] args) throws IOException{
		new Skeleton();
	}
}
