package teamawesome;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JPanel;

public class Menu extends JPanel {
	public static final int WINDOW_HEIGHT = 900;
	public static final int WINDOW_WIDTH = 600;
	public Menu(){
		setBackground(Color.WHITE);
		
	}
	@Override
	public void paint(Graphics g){
		//PAINT METHOD/
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(
		        RenderingHints.KEY_TEXT_ANTIALIASING,
		        RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
		Font font = new Font("Tahoma",Font.PLAIN,36);
		g2d.setFont(font);
		g2d.drawString("TEAM AWESOME",WINDOW_WIDTH/2,150);
		g2d.drawString("Paintball",WINDOW_WIDTH/2+60,200);
		g2d.drawString("Press Space to start...", WINDOW_WIDTH/2-30,300);
	}
}
